{-# LANGUAGE OverloadedStrings #-}
module CFMacros.Core (expandMacros) where

import           Control.Lens

import           Control.Arrow       (first)
import           Control.Monad.Catch (MonadThrow, throwM)
import           Data.Aeson.Lens     (key, members, _Object, _String)
import           Data.Maybe          (fromJust)
import           Data.Monoid         ((<>))
import           Data.Text           (Text)

import qualified Data.Aeson          as J
import qualified Data.HashMap.Strict as HashMap
import qualified Data.Text           as T

import           CFMacros.Types

--------------------------------------------------------------------------------

_StringVal :: Prism' J.Value J.Value
_StringVal =
  prism id (\v -> case v of J.String _ -> Right v; _ -> Left v)

--------------------

isParamReplacement :: Text -> Bool
isParamReplacement input =
  "{{" `T.isPrefixOf` input
    && "}}" `T.isSuffixOf` input

evalParamReplacement
  :: (Monad m, MonadThrow m)
     => MacroInputName -> MacroInput -> [Text] -> m AttrValue
evalParamReplacement macroInputName macroInputObj fields =
  case macroInputObj ^? foldr ((.) . key) id fields of
    Nothing ->
      let
        fieldName = T.intercalate "." fields
      in
        throwM $ MacroParamMissing macroInputObj macroInputName fieldName

    Just val ->
      return val

--------------------

hasParamInterpolation :: Text -> Bool
hasParamInterpolation =
    (> 0) . fst . T.foldl' step (0 :: Int, False)
  where
    step (paramCount, sharp) c
      | c == '#' =
        (paramCount, True)

      | c == '{' && sharp =
        (paramCount, True)

      | c == '}' && sharp =
        (succ paramCount, False)

      | otherwise =
        (paramCount, sharp)

evalParamInterpolation
  :: MonadThrow m
     => MacroInputName -> MacroInput
     -> AttrName -> AttrValue
     -> m AttrValue
evalParamInterpolation macroInputName macroInputObj templateAttrName templateAttrVal =
  let
    searchInterpolation input
      | T.null input =
        return input

      | ("#{", input1) <- T.splitAt 2 input =
        interpolate T.empty input1 >>= searchInterpolation

      | otherwise =
        (T.take 1 input <>) <$> searchInterpolation (T.tail input)

    interpolate inputAttrName input
      | T.null input =
        throwM $ StringInterpolationError macroInputName templateAttrName templateAttrVal

      | T.head input == '}' = do
          let fieldName = T.splitOn "." inputAttrName
          inputAttrVal <- evalParamReplacement macroInputName macroInputObj fieldName
          case inputAttrVal of
            J.String output ->
              return $ output <> T.tail input

            _ ->
              throwM $ StringInterpolationInputTypeMismatch macroInputName
                                                            macroInputObj
                                                            inputAttrName
                                                            inputAttrVal
                                                            templateAttrName
                                                            templateAttrVal

      | otherwise =
        interpolate (inputAttrName <> T.take 1 input) (T.tail input)
  in
    case templateAttrVal of
      J.String input0 -> J.String <$> searchInterpolation input0
      _ -> error "This will never happen; only way to get here is if inputValue is String"


--------------------

isVarNameReplacement :: Text -> Bool
isVarNameReplacement input =
  "<<" `T.isPrefixOf` input
    && ">>" `T.isSuffixOf` input

--------------------

evalSpecialString
  :: MonadThrow m
     => MacroTemplate
     -> MacroInputName
     -> MacroInput
     -> Text
     -> J.Value
     -> m J.Value
evalSpecialString macroTemplate macroInputName macroInputObj templateAttrName templateAttrVal =
  case templateAttrVal of
    J.String attrStrVal
      | isParamReplacement attrStrVal ->
        let
          paramList = T.splitOn "." $ T.init $ T.init $ T.drop 2 attrStrVal
        in
          evalParamReplacement macroInputName macroInputObj paramList

      | isVarNameReplacement attrStrVal ->
        let
          varName = T.init $ T.init $ T.drop 2 attrStrVal
        in
          case macroTemplate ^? key varName of
            Nothing ->
              throwM $ VarNameNotFound macroInputName templateAttrName

            Just _ ->
              return (J.String $ macroInputName <> "_" <> varName)

      | hasParamInterpolation attrStrVal ->
        evalParamInterpolation macroInputName macroInputObj templateAttrName templateAttrVal

      | otherwise ->
        return templateAttrVal
    _ -> error "Expecting JSON String, got something else!"

evalSpecialStrings
  :: MonadThrow m => MacroInputName -> MacroInput -> MacroTemplate -> m MacroOutput
evalSpecialStrings macroInputName macroInputObj macroTemplate =
  itraverseOf (deep (members . _StringVal))
              (evalSpecialString macroTemplate macroInputName macroInputObj)
              macroTemplate

--------------------

deepMerge :: J.Value -> J.Value -> J.Value
deepMerge (J.Object obj1) (J.Object obj2) =
  J.Object
    $ foldr (\(k, v2) acc ->
            maybe (HashMap.insert k v2 acc)
                  (\v1 -> HashMap.insert k (deepMerge v1 v2) acc)
                  (HashMap.lookup k obj1))
          obj1
          (HashMap.toList obj2)
deepMerge _ val2 = val2

applyOverrides
  :: MonadThrow m => J.Value -> J.Value -> m J.Value
applyOverrides macroInputObj expandedMacro =
  case macroInputObj ^? key "Overrides" of
    Nothing -> return expandedMacro
    Just overrides -> return $ deepMerge expandedMacro overrides

--------------------

evalMacro
  :: MonadThrow m
     => MacroInputName
     -> MacroInput
     -> MacroTemplate
     -> m MacroOutput
evalMacro macroInputName macroInputObj macroTemplate = do
  expandedMacro <- evalSpecialStrings macroInputName macroInputObj macroTemplate
  applyOverrides macroInputObj expandedMacro

expandMacro
  :: MonadThrow m
     => MacroMap -> (MacroInputName, MacroInput) -> m [(Text, MacroOutput)]
expandMacro macros resource@(macroInputName, macroInputObj) = do
    case mMacro of
      -- This is not a macroTemplate, just return resource
      Nothing -> return [resource]
      Just macroTemplate -> do
        evaledMacro <- evalMacro macroInputName macroInputObj macroTemplate
        case evaledMacro of
          J.Object hsh -> do
            -- prepend macroTemplate input name into the expanded resources
            let macroInputNamePrefix = macroInputName <> "_"
            return
              $ map (first (macroInputNamePrefix <>))
              $ HashMap.toList hsh
          val ->
            -- only way we get at this point is if mMacroTemplateName is a Just value
            throwM $ ExpandedMacroIsNotObject (fromJust mMacroTemplateName) macroInputName val
  where
    mMacroTemplateName = macroInputObj ^? key "Type" . _String
    mMacro = do
      macroTemplateName <- mMacroTemplateName
      macros ^? key macroTemplateName

expandMacros1 :: MonadThrow m => ConfigMap -> MacroInput -> m MacroOutput
expandMacros1 configMapObj macroInputObj =
    case configMapObj ^? key "Macros" of
      Nothing -> throwM $ MacrosKeyMissing configMapObj
      Just macros ->
        case macroInputObj ^? key "Resources" . _Object of
          Nothing -> throwM $ ResourcesKeyMissing macroInputObj
          Just resources -> do
           expandedResources <- fmap (HashMap.fromList . concat)
                                     (mapM (expandMacro macros)
                                           (HashMap.toList resources))
           return
             $ macroInputObj
             & key "Resources" . _Object .~ expandedResources

expandMacros :: MonadThrow m => ConfigMap -> MacroInput -> m MacroOutput
expandMacros configMapObj =
  let
    loop Nothing macroInputObj =
      expandMacros1 configMapObj macroInputObj
        >>= loop (Just macroInputObj)

    loop (Just prevMacroOutputObj) macroInputObj
      | macroInputObj == prevMacroOutputObj =
        return macroInputObj

      | otherwise =
        loop Nothing macroInputObj
  in
    loop Nothing
