{-# LANGUAGE TemplateHaskell #-}
module CFMacros.Types where

import           Control.Lens

import           Control.Exception (Exception)
import           Data.Text         (Text)
import           Data.Typeable     (Typeable)

import qualified Data.Aeson        as J

-- Configuration JSON object that contains Macros
type ConfigMap = J.Value

-- A JSON object that contains a group of MacroTemplates, where
-- each key is a MacroTemplateName
type MacroMap = J.Value

-- A JSON object that represents a compound CF Resource before expansion,
-- to more primitive Resources
type MacroInput = J.Value

-- A JSON object that represents one of the result of evaling a MacroTemplate
-- with a MacroInput
type MacroOutput = J.Value

-- A JSON object where each key is the name of a primitive CF Resource (An entry
-- on a MacroMap)
type MacroTemplate = J.Value

-- The name of a MacroTemplate JSON object
type MacroTemplateName = Text

-- The name that is going to be prefixed on each MacroTemplateName; this is
-- the name of the Compound CF Resource
type MacroInputName = Text

type AttrName = Text
type AttrValue = J.Value

-- The name of an Attribute in the InputMacro
type InputAttrName = AttrName

-- The value of an Attribute in an InputMacro
type InputAttrValue = AttrValue

data CFMacroError
     = MacroParamMissing {
       _errMacroInput            :: !MacroInput
     , _errMacroInputName        :: !MacroInputName
     , _errMacroInputAttrName    :: !AttrName
     }
     | ExpandedMacroIsNotObject {
       _errMacroInputName    :: !MacroInputName
     , _errMacroTemplateName :: !MacroTemplateName
     , _errMacroTemplate     :: !MacroTemplate
     }
     | StringInterpolationInputTypeMismatch {
        _errMacroInputName         :: !MacroInputName
     ,  _errMacroTemplate          :: !MacroTemplate
     ,  _errMacroInputAttrName     :: !AttrName
     ,  _errMacroInputAttrValue    :: !AttrValue
     ,  _errMacroTemplateAttrName  :: !AttrName
     ,  _errMacroTemplateAttrValue :: !AttrValue
     }
     | StringInterpolationError {
        _errMacroInputName        :: !MacroInputName
     ,  _errMacroInputAttrName    :: !AttrName
     -- ,  _errMacroTemplateName     :: !MacroTemplateName
     ,  _errMacroInput            :: !MacroInput
     }
     | VarNameNotFound {
        _errMacroInputName     :: !MacroInputName
     ,  _errMacroInputAttrName :: !AttrName
     }
     | MacrosFileNotFound {
     }
     | MacrosKeyMissing {
       _errConfigMap :: !ConfigMap
     }
     | InvalidResourcesInput {
     }
     | ResourcesKeyMissing {
       _errMacroInput :: !MacroInput
     }
     deriving (Show, Typeable)

instance Exception CFMacroError

$(makeLenses ''CFMacroError)
$(makePrisms ''CFMacroError)
