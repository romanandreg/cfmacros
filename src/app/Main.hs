module Main where

-- import Control.Lens
-- import Control.Exception.Lens

import Control.Monad (unless, when)
import Data.Aeson.Encode.Pretty (encodePretty)
import Data.Maybe (fromMaybe)
import Data.Typeable (typeOf)
import System.Directory (createDirectoryIfMissing, copyFile, doesDirectoryExist, doesFileExist)
import System.FilePath (joinPath)
import System.IO (stderr, hPutStrLn)
import System.Exit (exitFailure)

import qualified Data.Aeson as J
import qualified Data.Aeson.Encode as J (encode)
import qualified Data.ByteString.Lazy.Char8 as LB
import qualified Text.PrettyPrint.ANSI.Leijen as P

import Paths_cfmacros
import qualified Config
import OptParser
import CFMacros.Core

--------------------------------------------------------------------------------

readMacrosFile :: FilePath -> IO LB.ByteString
readMacrosFile home =
  LB.readFile (joinPath [home, "Macros.json"])

setupHome :: IO FilePath
setupHome = do
    mInputHome <- Config.safeGetEnv "CFMACROS_HOME"
    case mInputHome of
      Nothing -> do
        sampleFilePath <- getDataFileName "Macros.json"
        defaultHome <- Config.getDefaultCFMacrosHome
        homeExists <- doesDirectoryExist defaultHome
        unless homeExists $ do
          createDirectoryIfMissing True defaultHome
          let configHomePath = joinPath [defaultHome, "Macros.json"]
          copyFile sampleFilePath configHomePath
        return defaultHome
      Just inputHome -> do
        homeExists <- doesDirectoryExist inputHome
        unless homeExists $ do
          P.hPutDoc stderr
            $ P.text "Home config specified on CFMACROS_HOME "
              P.<+> P.red (P.dquotes $ P.text inputHome)
              P.<+> P.text "is not present"
          exitFailure
        return inputHome

eitherDecode :: J.FromJSON o => Maybe String -> LB.ByteString -> IO o
eitherDecode mSource input = do
    case J.eitherDecode' input of
      Left err -> do
        hPutStrLn stderr ("Error: JSON found on \"" ++ source ++ "\" is invalid")
        exitFailure
      Right value -> return value
  where
    source = fromMaybe "stdin" mSource

main :: IO ()
main = do
  home <- setupHome
  cmd <- getProgramCommand
  case cmd of
    EvalMacro mFile prettyOutput -> do
      let configFile = joinPath [home, "Macros.json"]
      macrosInput <- LB.readFile configFile
      configMap <- eitherDecode (Just configFile) macrosInput

      cloudFormationInput <- maybe LB.getContents LB.readFile mFile
      cloudFormation <- eitherDecode mFile cloudFormationInput

      case expandMacros configMap cloudFormation of
        Left err -> print err
        Right result
          | prettyOutput -> LB.putStrLn (encodePretty result)
          | otherwise    -> LB.putStrLn (J.encode result)

    PrintVersion -> do
      hPutStrLn stderr "Feature pending ¯\\_(ツ)_/¯"
      exitFailure

    PrintConfig -> do
      hPutStrLn stderr "Feature pending ¯\\_(ツ)_/¯"
      exitFailure
