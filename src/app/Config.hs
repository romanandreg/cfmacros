{-# LANGUAGE ScopedTypeVariables #-}
module Config where

import Control.Exception (SomeException, try)
import System.Environment (getEnv)
import System.FilePath (joinPath)
import System.Directory (getHomeDirectory)


safeGetEnv :: String -> IO (Maybe String)
safeGetEnv name = do
  result <- try (getEnv name)
  case result of
    Left (e :: SomeException) -> return Nothing
    Right val -> return (Just val)

getDefaultCFMacrosHome :: IO FilePath
getDefaultCFMacrosHome = do
  home <- getHomeDirectory
  return (joinPath [home, ".config", "cfmacros"])

getCFMacrosHome :: IO FilePath
getCFMacrosHome = do
  mHome <- safeGetEnv "CFMACROS_HOME"
  maybe getDefaultCFMacrosHome return mHome

--------------------------------------------------------------------------------
-- Posible config library (Over-engineered crap)

-- data ConfigSource a
--      = Default !a
--      | EnvVar  !String !a
--      | CLI     !a
--      deriving (Show, Eq, Typeable)

-- instance Monoid a => Monoid (ConfigSource a) where
--   mempty = Default mempty
--   a `mappend` Default {} = a
--   Default {} `mappend` a  = a
--   a `mappend` _ = a

-- fromSource :: ConfigSource a -> a
-- fromSource (Default a) = a
-- fromSource (EnvVar  _ a) = a
-- fromSource (CLI a) = a

-- fetchEnvConfig :: String -> IO (Maybe (ConfigSource String))
-- fetchEnvConfig name = do
--   result <- try (getEnv name)
--   case result of
--     Left (_ :: SomeException) -> return Nothing
--     Right val -> return (Just (EnvVar name val))

-- defaultConfig :: a -> ConfigSource a
-- defaultConfig = Default

-- isDefaultConfig :: ConfigSource a -> Bool
-- isDefaultConfig (Default _) = True
-- isDefaultConfig _ = False

-- configSourceToString :: Show a => ConfigSource a -> String
-- configSourceToString (Default a) = show a ++ " (default)"
-- configSourceToString (EnvVar name a) = show a ++ " (" ++ name ++ ")"
-- configSourceToString (CLI a) = show a ++ " (cli)"

-- printConfig :: Show a => Config.ConfigSource a -> IO ()
-- printConfig macrosHome =
--     P.putDoc $ P.vsep titles P.<+> P.vsep descs P.<> P.linebreak
--   where
--     configList0 = [("Home Directory:", configSourceToString macrosHome)]
--     (titles, descs) = foldr (\(title, desc) (titles1, descs1) ->
--                               (P.text title : titles1, P.text desc : descs1))
--                               ([], [])
--                               configList0

-- --------------------------------------------------------------------------------

-- getDefaultCFMacrosHome :: IO FilePath
-- getDefaultCFMacrosHome = do
--   home <- getHomeDirectory
--   return (joinPath [home, ".config", "cfmacros"])

-- getCFMacrosHome :: IO (ConfigSource FilePath)
-- getCFMacrosHome = do
--   mHome <- fetchEnvConfig "CFMACROS_HOME"
--   maybe (defaultConfig <$> getDefaultCFMacrosHome) return mHome
