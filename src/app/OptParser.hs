module OptParser where

import Data.List (intercalate)
import Options.Applicative

data CFMacrosCmd
  = EvalMacro { sourceFilePath   :: Maybe FilePath
              , prettyJsonOutput :: Bool }
  | PrintConfig
  | PrintVersion
  deriving (Show, Eq)

inputFile :: Parser CFMacrosCmd
inputFile =
  EvalMacro
    <$> optional (argument str
                   $ metavar "FILE"
                   <> help "CloudFormation JSON filepath, use stdin if not given")
    <*> switch (long "pretty"
                <> short 'p'
                <> help "Pretty Print CloudFormation JSON output")

evalCmdHelpMsg :: String
evalCmdHelpMsg =  "Evals macros on a CF JSON file and prints to stdout"

evalCmd :: Mod CommandFields CFMacrosCmd
evalCmd =
  command "eval"
    $ info inputFile
           (fullDesc
            <> progDesc evalCmdHelpMsg)


configCmdHelpMsg :: String
configCmdHelpMsg =  "Prints configuration settings"

configCmd :: Mod CommandFields CFMacrosCmd
configCmd =
  command "config"
    $ info (pure PrintConfig)
           (fullDesc
            <> progDesc configCmdHelpMsg)

cfmacrosParser :: Parser CFMacrosCmd
cfmacrosParser =
  hsubparser (evalCmd <> configCmd)
  <|> flag' PrintVersion (long "version"
                          <> short 'v'
                          <> help "Prints current version and exits")

getProgramCommand :: IO CFMacrosCmd
getProgramCommand = execParser opts
  where
    opts = info (helper <*> cfmacrosParser)
                (fullDesc
                <> progDesc
                    "Simplify your CloudFormation JSON files by using high level macros"
                <> header "cfmacros - expand CloudFormation macros")
