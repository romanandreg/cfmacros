{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
module CFMacros.CoreTest (tests) where

import Control.Exception.Lens
import Data.Aeson.QQ
import qualified Data.Aeson as J

import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (Assertion, testCase, assertBool, assertEqual, assertFailure)

import CFMacros.Types
import CFMacros.Core

--------------------------------------------------------------------------------

assertMacro :: MacroOutput -> MacroMap -> MacroInput -> Assertion
assertMacro expected configMap input =
    case expandMacros configMap input of
      Left err -> assertFailure (show err)
      Right output -> assertEqual "Macro didn't expand correctly" expected output

-- assertMacroFailure :: String -> ConfigMap -> MacroInput -> Assertion
assertMacroFailure selectorLens f configMap input = do
  result <- trying selectorLens (expandMacros configMap input)
  case result of
    Left err ->
      assertBool ("Assertion function returned false for error: " ++ show err) (f err)

    Right _ ->
      assertFailure $ "Expecting macro failure, but it succeeded"

tests :: TestTree
tests =
  testGroup
    "CoreTest"
    [
      testCase "1 macro maps to 1 resource"
        $ assertMacro [aesonQQ| { "Resources": { "sampleInput_resource": "FooBar"  } } |]
                      [aesonQQ|
                        {"Macros": {
                          "MacroA": {
                            "resource": "{{Properties.HelloWorld}}"
                          }}} |]
                      [aesonQQ|
                        { "Resources": {
                          "sampleInput": {
                            "Type": "MacroA",
                            "Properties": {"HelloWorld": "FooBar"}}
                          }
                        } |]

    , testCase "1 macro maps to many resources"
        $ assertMacro [aesonQQ|
                        { "Resources": {
                            "sampleInput_resourceA": {"attr": "Foo"},
                            "sampleInput_resourceB": {"other": "Bar"}
                            }} |]
                      [aesonQQ|
                        { "Macros": {
                            "ManyMacros": {
                              "resourceA": {"attr": "{{Properties.FooName}}"},
                              "resourceB": {"other": "{{Properties.BarName}}"}
                            }}} |]
                      [aesonQQ|
                        { "Resources": {
                            "sampleInput": {
                              "Type": "ManyMacros",
                              "Properties": {"FooName": "Foo",
                                             "BarName": "Bar"}
                            }}}  |]

    , testCase "Properties can be deeply nested on macro template"
        $ assertMacro [aesonQQ|
                        { "Resources": {
                            "sampleInput_resourceA": {"one": {"two": {"three": "Nested"}}}
                            }} |]
                      [aesonQQ|
                        { "Macros": {
                            "NestedMacro": {
                              "resourceA": {"one": {"two": {"three": "{{Properties.Name}}"}}}
                            }}} |]
                      [aesonQQ|
                        { "Resources": {
                            "sampleInput": {
                              "Type": "NestedMacro",
                              "Properties": {"Name": "Nested"}
                            }}}  |]

    , testCase "Properties input may be of different types"
        $ assertMacro [aesonQQ|
                        { "Resources": {
                            "sampleInput_resourceA": {
                              "One": 1,
                              "Two": true,
                              "Three": {"Nested": [1,2,false]}
                            }}} |]
                      [aesonQQ|
                        { "Macros": {
                            "MultipleTypes": {
                              "resourceA": {
                                "One": "{{Properties.One}}",
                                "Two": "{{Properties.Two}}",
                                "Three": "{{Properties.Three}}"
                             }}}} |]
                      [aesonQQ|
                        { "Resources": {
                            "sampleInput": {
                              "Type": "MultipleTypes",
                              "Properties": {
                                "One": 1,
                                "Two": true,
                                "Three": {"Nested": [1,2,false]}
                              }}}}  |]

    , testCase "Unknown Resource 'Type' won't throw any error"
        $ assertMacro [aesonQQ|
                        { "Resources": {
                            "sampleInput": {
                              "Type": "UnknownMacro",
                              "Properties": {
                                "Hello": "World"
                              }
                            }}} |]
                      [aesonQQ| { "Macros": {}} |]
                      [aesonQQ|
                        { "Resources": {
                            "sampleInput": {
                              "Type": "UnknownMacro",
                              "Properties": {
                                "Hello": "World"
                              }
                            }}}  |]

    , testCase "Macros without a 'Macros' key causes failure"
        $ let macrosObj = [aesonQQ| { "FooBar": {}} |]
          in assertMacroFailure (exception . _MacrosKeyMissing)
                                (== macrosObj)
                                macrosObj
                                [aesonQQ| { "Resources": {}}  |]

    , testCase "Input without a 'Resources' key causes failure"
        $ let resourceObj = [aesonQQ| { "FooBar": {}}  |]
          in assertMacroFailure (exception . _ResourcesKeyMissing)
                                (== resourceObj)
                                [aesonQQ| { "Macros": {}} |]
                                resourceObj

    , testCase "Overwrites modify values of template with any new type"
        $ assertMacro [aesonQQ|
                        { "Resources": {
                            "sampleInput_resourceA": {
                              "One": 123,
                              "Two": true,
                              "Three": {"Nested": [1,2,false]}
                            }}} |]
                      [aesonQQ|
                        { "Macros": {
                            "MultipleTypes": {
                              "resourceA": {
                                "One": "Constant 1",
                                "Two": "Constant 2",
                                "Three": "Constant 3"
                             }}}} |]
                      [aesonQQ|
                        { "Resources": {
                            "sampleInput": {
                              "Type": "MultipleTypes",
                              "Properties": {},
                              "Overrides":
                                {"resourceA": {
                                    "One": 123,
                                    "Two": true,
                                    "Three": {"Nested": [1,2,false]
                                  }}
                                }}}}  |]

    , testCase "Fails when all Properties are not specified on input resource"
        $ assertMacroFailure (exception . _MacroParamMissing)
                             (\(_, _, attrName) -> attrName == "Properties.One")
                             [aesonQQ|
                               { "Macros": {
                                   "MultipleTypes": {
                                     "resourceA": {
                                       "One": "{{Properties.One}}"
                                    }}}} |]
                             [aesonQQ|
                               { "Resources": {
                                   "sampleInput": {
                                     "Type": "MultipleTypes",
                                     "Properties": {}
                                   }}}  |]

    , testCase "String interpolation works"
        $ assertMacro [aesonQQ|
                        { "Resources": {
                            "sampleInput_resourceA": {
                              "Foo": "hello_world"
                            }}} |]
                      [aesonQQ|
                        { "Macros": {
                            "StringInterpolation": {
                              "resourceA": {
                                "Foo": "#{Properties.One}_#{Properties.Two}"
                             }}}} |]
                      [aesonQQ|
                        { "Resources": {
                            "sampleInput": {
                              "Type": "StringInterpolation",
                              "Properties": {
                                "One": "hello",
                                "Two": "world"
                              }}}}  |]

    , testCase "String interpolation only works on Strings"
        $ assertMacroFailure (exception . _StringInterpolationInputTypeMismatch)
                             (\(_, _, inputAttrName, inputAttrVal, _, _) ->
                               inputAttrName == "Properties.One"
                               && inputAttrVal == J.Number 123)
                             [aesonQQ|
                               { "Macros": {
                                   "StringInterpolation": {
                                     "resourceA": {
                                       "Foo": "#{Properties.One}_#{Properties.Two}"
                                    }}}} |]
                             [aesonQQ|
                               { "Resources": {
                                   "sampleInput": {
                                     "Type": "StringInterpolation",
                                     "Properties": {
                                       "One": 123,
                                       "Two": "world"
                                     }}}}  |]

    , testCase "Resource name replacement works"
        $ assertMacro [aesonQQ|
                        { "Resources": {
                            "sampleInput_resourceA": {
                              "Foo": "hello_world"
                            },
                            "sampleInput_resourceB": {
                              "Bar": "sampleInput_resourceA"
                            }}} |]
                      [aesonQQ|
                        { "Macros": {
                            "StringInterpolation": {
                              "resourceA": {
                                "Foo": "#{Properties.One}_#{Properties.Two}"
                              },
                              "resourceB": {
                                "Bar": "<<resourceA>>"
                              }}}} |]
                      [aesonQQ|
                        { "Resources": {
                            "sampleInput": {
                              "Type": "StringInterpolation",
                              "Properties": {
                                "One": "hello",
                                "Two": "world"
                              }}}}  |]

    , testCase "Non existing resource name replacement fails"
        $ assertMacroFailure (exception . _VarNameNotFound)
                             (\(_, fieldName) -> fieldName == "Foo")
                             [aesonQQ|
                               { "Macros": {
                                   "StringInterpolation": {
                                     "resourceA": {
                                       "Foo": "<<Foobar>>"
                                    }}}} |]
                             [aesonQQ|
                               { "Resources": {
                                   "sampleInput": {
                                     "Type": "StringInterpolation",
                                     "Properties": {
                                       "One": 123,
                                       "Two": "world"
                                     }}}}  |]

    ]
