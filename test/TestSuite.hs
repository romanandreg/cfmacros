module Main where

import Test.Tasty (defaultMain, testGroup)

import qualified CFMacros.CoreTest

main :: IO ()
main = defaultMain (testGroup "CFMacros" [CFMacros.CoreTest.tests])

